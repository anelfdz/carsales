package com.carsales.ui.common

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
