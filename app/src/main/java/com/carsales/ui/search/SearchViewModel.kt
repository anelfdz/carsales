package com.carsales.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.carsales.repository.CarRepository
import com.carsales.util.AbsentLiveData
import com.carsales.data.vo.Car
import com.carsales.data.vo.Resource
import javax.inject.Inject

class SearchViewModel @Inject constructor(carRepository: CarRepository) : ViewModel() {

    private val _request = MutableLiveData<Boolean>()

    private val _car = MutableLiveData<Car>()

    val results: LiveData<Resource<List<Car>>> = Transformations
        .switchMap(_request) {
            if (it == null || !it) {
                AbsentLiveData.create()
            } else {
                carRepository.search()
            }
        }

    val saveCarResult: LiveData<Resource<Any>> = Transformations
        .switchMap(_car) {
            if (it == null) {
                AbsentLiveData.create()
            } else {
                carRepository.saveCar(it, "token")
            }
        }

    fun loadCarList() {
        _request.value = true
    }

    fun retry() {
        _request.value = true
    }

    fun saveCar(car: Car) {
        if (_car.value != car) {
            _car.value = car
        }
    }
}