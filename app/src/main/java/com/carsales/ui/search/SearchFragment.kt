package com.carsales.ui.search

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.carsales.AppExecutors
import com.carsales.R
import com.carsales.binding.FragmentDataBindingComponent
import com.carsales.databinding.FragmentSearchBinding
import com.carsales.ui.common.BaseFragment
import com.carsales.ui.common.RetryCallback
import com.carsales.util.autoCleared
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject


class SearchFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    var binding by autoCleared<FragmentSearchBinding>()

    var adapter by autoCleared<CarListAdapter>()

    lateinit var searchViewModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search,
            container,
            false,
            dataBindingComponent
        )

        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(SearchViewModel::class.java)
        binding.setLifecycleOwner(viewLifecycleOwner)
        initRecyclerView()

        val rvAdapter = CarListAdapter(
            dataBindingComponent = dataBindingComponent,
            appExecutors = appExecutors
        ) { car ->
            searchViewModel.saveCar(car)
        }

        binding.carList.adapter = rvAdapter
        adapter = rvAdapter

        searchViewModel.saveCarResult.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.root, "Status: ${it.status} ${it.message}", Snackbar.LENGTH_LONG).show()
        })

        binding.callback = object : RetryCallback {
            override fun retry() {
                searchViewModel.retry()
            }
        }

        searchViewModel.loadCarList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_fragment, menu)
    }

    private fun initRecyclerView() {
        binding.searchResult = searchViewModel.results
        searchViewModel.results.observe(viewLifecycleOwner, Observer { result ->
            adapter.submitList(result?.data)
        })
    }
}
