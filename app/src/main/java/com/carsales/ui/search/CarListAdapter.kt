package com.carsales.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.carsales.AppExecutors
import com.carsales.R
import com.carsales.data.vo.Car
import com.carsales.databinding.CarSingleItemBinding
import com.carsales.ui.common.DataBoundListAdapter

class CarListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val saveCarClickCallback: ((Car) -> Unit)?
    ) : DataBoundListAdapter<Car, CarSingleItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Car>() {
        override fun areItemsTheSame(oldItem: Car, newItem: Car): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Car, newItem: Car): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.price == newItem.price
                    && oldItem.imageUrl == newItem.imageUrl
        }
    }
) {

    override fun createBinding(parent: ViewGroup): CarSingleItemBinding {
        val binding = DataBindingUtil.inflate<CarSingleItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.car_single_item,
            parent,
            false,
            dataBindingComponent
        )
        binding.ivSaveCar.setOnClickListener {
            binding.car?.let { car -> saveCarClickCallback?.invoke(car) }
        }
        return binding
    }

    override fun bind(binding: CarSingleItemBinding, item: Car) {
        binding.car = item
    }
}