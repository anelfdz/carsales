package com.carsales.repository

import androidx.lifecycle.LiveData
import com.carsales.AppExecutors
import com.carsales.api.ApiResponse
import com.carsales.api.CarsalesApi
import com.carsales.data.dto.SearchCarsResponse
import com.carsales.data.vo.Car
import com.carsales.data.vo.Resource
import com.carsales.util.AbsentLiveData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CarRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val carsalesApi: CarsalesApi
) {

    fun search(): LiveData<Resource<List<Car>>> {
        return object : NetworkBoundResource<List<Car>, SearchCarsResponse>(appExecutors) {
            override fun saveCallResult(item: SearchCarsResponse) {

            }

            override fun shouldFetch(data: List<Car>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Car>> = AbsentLiveData.create()

            override fun createCall(): LiveData<ApiResponse<SearchCarsResponse>> = carsalesApi.searchCars()

            override fun mapResponse(body: SearchCarsResponse): List<Car> = body.result
        }.asLiveData()
    }

    fun saveCar(car: Car, token: String): LiveData<Resource<Any>> {
        return object : NetworkBoundResource<Any, Any>(appExecutors) {
            override fun saveCallResult(item: Any) {

            }

            override fun shouldFetch(data: Any?): Boolean = true

            override fun loadFromDb(): LiveData<Any> = AbsentLiveData.create()

            override fun createCall(): LiveData<ApiResponse<Any>> = carsalesApi.saveCar("Bearer $token", car.id)

        }.asLiveData()
    }
}
