package com.carsales.api

import androidx.lifecycle.LiveData
import com.carsales.data.dto.SearchCarsResponse
import com.carsales.data.vo.Car
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface CarsalesApi {

    companion object {
        val BASE_URL = "https://retail-api.prd.latam.csnglobal.net"
    }

    @GET("/chileautos/v1/stock/listing")
    fun searchCars(): LiveData<ApiResponse<SearchCarsResponse>>

    @POST("/chileautos/v1/saveditem/{id}")
    fun saveCar(@Header("Authorization") token: String, @Path("id") id: String) : LiveData<ApiResponse<Any>>
}