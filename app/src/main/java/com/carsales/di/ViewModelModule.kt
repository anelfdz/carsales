package com.carsales.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.carsales.ui.search.SearchViewModel
import com.carsales.viewmodel.CarsalesViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: CarsalesViewModelFactory): ViewModelProvider.Factory
}
