package com.carsales.binding

import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.facebook.drawee.view.SimpleDraweeView
import javax.inject.Inject

class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    @BindingAdapter("imageUrl")
    fun bindImage(imageView: SimpleDraweeView, url: String?) {
        imageView.setImageURI(url)
    }
}
