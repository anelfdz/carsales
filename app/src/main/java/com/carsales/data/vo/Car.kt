package com.carsales.data.vo

import com.google.gson.annotations.SerializedName

data class Car(
    @SerializedName("networkId")
    val id: String,

    @SerializedName("mainPhoto")
    val imageUrl: String,

    @SerializedName("displayTitle")
    val title: String,

    @SerializedName("displayPrice")
    val price: Price
)
