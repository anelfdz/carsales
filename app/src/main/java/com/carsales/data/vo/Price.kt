package com.carsales.data.vo

import com.google.gson.annotations.SerializedName

data class Price(
    @SerializedName("price")
    val price: String,

    @SerializedName("preInfo")
    val info: String
)
