package com.carsales.data.dto

import com.carsales.data.vo.Car
import com.google.gson.annotations.SerializedName

data class SearchCarsResponse(
    @SerializedName("result")
    val result: List<Car>
)
